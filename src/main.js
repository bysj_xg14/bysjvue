//webpack的入口!!!!!!

import Vue from 'vue';
import App from './App';   //引入App.vue
import router from './router';  //引入router路由
import VueRouter from 'vue-router';
import iView from 'iview';   //引入iview
import 'iview/dist/styles/iview.css';    //引入iview的一些css
import axios from 'axios';


Vue.prototype.$http = axios

Vue.config.productionTip = false  //不需要Vue提示信息
Vue.use(VueRouter)
Vue.use(iView)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>',
})
