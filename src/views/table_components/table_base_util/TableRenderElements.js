// Render 函数
// 传入两个参数，第一个是 h---也就是createElement，第二个为对象param
// 对象包含 row、column 和 index，分别指当前行数据，当前列数据，当前行索引


//引入单文件组件
import tableSelect from '../table_base_components/TableSelect';
import tableInputNum from '../table_base_components/InputNum';
import datePicker from '../table_base_components/DatePicker';
// import tableInputNum from '../table_base_components/InputNum';

//获取相应的render元素
const tableEles = (vm, h, param, item) => {
    switch (item.cellType){
        case "object":
            return h('Col', //div也行
                [
                    rowSelect(vm, h, param, item)
                ]);
            break;
        case "object_leader":
            return h('Col', //div也行
                [
                    rowSelect(vm, h, param, item)
                ]);
            break;
        case "object_edit":
            return h('Col', //div也行
                [
                    rowSelect(vm, h, param, item)
                ]);
            break;
         case "object_check":
            return h('Col', //div也行
                [
                    rowSelect(vm, h, param, item)
                ]);
            break;
        case "textarea":
            return h('Col',
            [
                rowMInput(vm, h, param, item),
            ]);
            break;
        case "datePicker":
            return h('Col',//div也行
            [
                rowDatepicker(vm, h, param, item),
            ]);
            break;
         case "inputNum_style":
            return h('Col',
            [
                rowInputNum(vm, h, param, item),
            ]);
            break;
        case "pass_switch"://审核开题报告用到的通过开关
            return h('col',
            [
                rowSwitch(vm,h,param,item),
            ]);
            break;
        case "approvalFromDirector":
            return h('Col', //div也行
                [
                    rowSelect(vm, h, param, item)
                ]);
            
        case "mobject_questionTeam":
            return h('Col', //div也行
                [
                        rowMultipleSelect(vm, h, param, item)
                ]);
        case "mobject":
            return h('Col', //div也行
                [
                        rowMultipleSelect(vm, h, param, item)
                ]);
        default:
            return h('div',
            [
                rowInput(vm, h, param, item)
            ]);

    };
};
//单选下拉框
const rowSelect = (vm, h, param, item) => {
    return h(tableSelect, {
        props: {
            multipleState: false, //单选
            selectUrl: vm.allSelectUrls[item.key],   //下拉框URL
            getValue:  vm.value[param.index][item.key],   //绑定值
        },
        on: {
            'select-onChange' (value) {
                let key = param.column.key;
                vm.thisTableData[param.index][key] = value;
            }
        }
    });
};
//多选下拉框
const rowMultipleSelect = (vm, h, param, item) => {
    return h(tableSelect, {
        props: {
            multipleState: true, //单选
            selectUrl: vm.allSelectUrls[item.key],   //下拉框URL
            getValue:  vm.value[param.index][item.key],   //绑定值
        },
        on: {
            'select-onChange' (value) {
                let key = param.column.key;
                vm.thisTableData[param.index][key] = value;
            }
        }
    });
};
//日期选择器
const rowDatepicker = (vm,h,param,item) => {
    return h(datePicker,{
        props: {
            type: 'text',
            value: vm.value[param.index][item.key] //框里要显示着值
        },
        on: {
            'select-onChange' (value) {
                let key = param.column.key;
                vm.thisTableData[param.index][key] = value;
            }
        }

    });
};
const showValue = (vm, h, param, item) => {
    return h('label', {
         attrs: {
            text:  vm.value[param.index][item.key],
        }
    });
};
//Input框
const rowInput = (vm, h, param, item) => {
    return h('Input', {
        props: {
            type: 'text',
            value: vm.value[param.index][item.key] //框里要显示着值
        },
        attrs: {
            id:param.column.key,//这个id居然在input框外层的div上,
            placeholder: "不可为空..",
        },
        on: {
            'on-change' (event) {
                let key = param.column.key;
                vm.thisTableData[param.index][key] = event.target.value;
            }
        }
    });
};
//textarea
const rowMInput = (vm, h, param, item) => {
    return h('Input', {
        props: {
            type: 'textarea',
            value: vm.value[param.index][item.key] //框里要显示着值
        },
        attrs: {
            placeholder: "不可为空..",
        },
        on: {
            'on-change' (event) {
                let key = param.column.key;
                vm.thisTableData[param.index][key] = event.target.value;
            }
        }
    });
};
const rowInputNum = (vm, h, param, item) => {
        return h('InputNumber', {
            props: {
                max: 2019,
                min: 2017,
                value:2018
            },
            data(){
                return{
                    value:Number
                }
            },
            on:{
             'on-change'(value){
                 let key = param.column.key;
                 console.log(key);
                 vm.thisTableData[param.index][key] = value;
                 console.log("===========================================");
                 console.log(vm.thisTableData[param.index][key]);
             }
            }
        });
    };

export default tableEles;
