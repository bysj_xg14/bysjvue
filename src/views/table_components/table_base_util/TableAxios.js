import axios from 'axios';

import uploadbutton from '@/views/button/UploadButton';
//全局定义axios字段
axios.defaults.headers.put['Content-Type'] = 'application/json;charset=UTF-8';
axios.defaults.headers.post['Content-Type'] = 'application/json;charset=UTF-8';
axios.defaults.headers.delete['Content-Type'] = 'application/json;charset=UTF-8';
//get
const axiosGet = (url, componentVm) => {
  axios.get(url)
    .then(function (response) {
      if (response.status === 200) {
        componentVm.tableDataList = response.data.rows;
        componentVm.pageTotals = response.data.total;

        //判断老师是否分配学生
        if (componentVm.graduateProject != undefined) {
          componentVm.graduateProject = componentVm.tableDataList[0].graduateProject == null ? false : true
        }
        //判断老师是否下达任务书
        if (componentVm.missionDoc != undefined) {
          componentVm.missionDoc = componentVm.tableDataList[0].graduateProject.missionDoc.file == null ? false : true
        }
        //判断老师是否下达任务书
        if(componentVm.missionDoc!=undefined){
        componentVm.missionDoc = componentVm.tableDataList[0].graduateProject.missionDoc.file==null?false:true
         console.log(componentVm.tableDataList[0].graduateProject.missionDoc)
         console.log(componentVm.tableDataList[0].graduateProject.missionDoc.file)
         }
         //判断教研室主任是否安排答辩时间地点
          componentVm.timeAndAddress = componentVm.tableDataList==null?false:true

      } else {
        componentVm.$Message.error("数据显示失败");
      }
    })
    // .catch(function (error) {
    //   componentVm.$Message.error({
    //     content: error,
    //     duration: 20,  //至少 维持20秒.20秒不关. 自己关闭
    //     closable: true  //设为true这个框就可以自己关闭
    //   });
    // });
};
// put Array
const axiosArrayPut = (url, componentVm, currentVal) => {
  axios.put(url, currentVal)
    .then(function (response) {
      if (response.status === 200) {
        componentVm.tableDataList = response.data.rows;
        componentVm.pageTotals = response.data.total;
        componentVm.$Message.success({
          content: '数据操作成功',
          duration: 5,
        });
      } else {
        componentVm.$Message.error("数据操作失败");
      }
    })
    .catch(function (error) {
      componentVm.$Message.error({
        content: error,
        duration: 20,  //至少 维持20秒.20秒不关. 自己关闭
        closable: true  //设为true这个框就可以自己关闭
      });
    });
}


//post
//put
const axiosPut = (url, componentVm, currentVal) => {
  axios.put(url, currentVal)
    .then(function (response) {
      if (response.status === 200) {
        componentVm.tableDataList = response.data.rows;
        componentVm.pageTotals = response.data.total;
        componentVm.$Message.success({
          content: '数据操作成功',
          duration: 5,
        });
      } else {
        componentVm.$Message.error("数据操作失败");
      }
    })
    .catch(function (error) {
      componentVm.$Message.error({
        content: error,
        duration: 20,  //至少 维持20秒.20秒不关. 自己关闭
        closable: true  //设为true这个框就可以自己关闭
      });
    });
}
//delete   delete和put post不一样,注意一下
const axiosDelete = (url, componentVm, deleteVals) => {        //到时候要把表格数据再传进来一次
  axios.delete(url, {
      data: deleteVals
    }
  )
    .then(function (response) {
      if (response.status === 200) {
        componentVm.tableDataList = response.data.rows;
        componentVm.pageTotals = response.data.total;
        componentVm.$Message.success({
          content: '数据删除成功',
          duration: 5,
        });
      } else {
        componentVm.$Message.error("数据删除失败");
      }
    })
    .catch(function (error) {
      componentVm.$Message.error({
        content: error,
        duration: 20,  //至少 维持20秒.20秒不关. 自己关闭
        closable: true  //设为true这个框就可以自己关闭
      });
    });
}


//获取下拉框的url
const selectUrl = (vm, selectName) => {
  let url = vm.allSelectUrls[selectName];
  return url;
};
const showStudentsFromTeacherButton = (vm, h, currentRow, index) => {
  return h('Button', {
    props: {
      type: 'primary',
      size: 'small'
    },
    style: {
      marginRight: '5px'
    },
    on: {
      click: () => {
        var teacherId = currentRow.id;
        axios.get("/getAllStudentNamesByTeacher?"+  "teacherId=" + teacherId)
          .then(function (response) {
            if (response.status = 200) {
              var students = "";
              if(response.data.length==0){
                students="无";
              }else{
                  for(var i = 0;i<response.data.length;i++){
                    students+=response.data[i].value;
                    if(i!=response.data.length-1){
                        students+=",";
                    }
                  }
              }
              vm.$Modal.info({
                title: '显示详情',
                content: `已分配学生姓名：${students}<br>`
              })
            }
          })
      }
    }
  }, '显示已经匹配的学生')
};
// 分配学生显示按钮
const showButton = (vm, h, currentRow, index) => {
  return h('Button', {
    props: {
      type: 'primary',
      size: 'small'
    },
    style: {
      marginRight: '5px'
    },
    on: {
      click: () => {
        console.log("11111111");
        // 题目
        var title = null;
        if (currentRow.title == null) {
          title = "无"
        } else {
          title = currentRow.title
        }
        // 副标题
        var subTitle = null;
        if (currentRow.subTitle == null) {
          subTitle = "无"
        } else {
          subTitle = currentRow.subTitle
        }
        // 题目类型
        var graduateProjectType = null;
        if (currentRow.graduateProjectType == null) {
          graduateProjectType = "无";
        } else {
          graduateProjectType = currentRow.graduateProjectType.value;
        }
        // 题目来源
        var graduateProjectFrom = null;
        if (currentRow.graduateProjectFrom == null) {
          graduateProjectFrom = "无";
        } else {
          graduateProjectFrom = currentRow.graduateProjectFrom.value;
        }
        // 题目性质
        var graduateProjectNature = null;
        if (currentRow.graduateProjectNature == null) {
          graduateProjectNature = "无";
        } else {
          graduateProjectNature = currentRow.graduateProjectNature.value;
        }
        // 年份
        var year = null;
        if (currentRow.year == null) {
          year = "无";
        } else {
          year = currentRow.year;
        }
        // 工作内容
        var content = null;
        if (currentRow.content == null) {
          content = "无";
        } else {
          content = currentRow.content;
        }
        // 基本需求
        var requirements = null;
        if (currentRow.requirements == null) {
          requirements = "无";
        } else {
          requirements = currentRow.requirements;
        }
        // 所需技能
        var basicString = null
        if (currentRow.basicString == null) {
          basicString = "无";
        } else {
          basicString = currentRow.basicString;
        }
        // 参考文献
        var reference = null;
        if (currentRow.reference == null) {
          reference = "无";
        } else {
          reference = currentRow.reference;
        }
        // 学生姓名
        var student = null;
        if (currentRow.student == null) {
          student = "无";
        } else {
          student = currentRow.student.value;
        }
        // 指导老师姓名
        var supervisor = null;
        if (currentRow.student == null) {
          supervisor = "无";
        } else if (currentRow.student.supervisor == null) {
          supervisor = "无";
        }
        else {
          supervisor = currentRow.student.supervisor.value;
        }
        // 类别
        var category = null;
        if (currentRow.category == null) {
          category = "无";
        } else {
          category = currentRow.category;
        }
        // 班级
        var studentClass = null;
        if (currentRow.student == null) {
          studentClass = "无";
        } else {
          studentClass = currentRow.student.studentClass.value;
        }
        // this.ifNull(student);
        vm.$Modal.info({
          title: '显示详情',
          content: `姓名：${student}
          <br>班级：${studentClass}
          <br>指导教师：${supervisor}
          <br>题目：${title}
          <br>副标题：${subTitle}
          <br>题目类型：${graduateProjectType}
          <br>题目性质：${graduateProjectNature}
          <br>题目来源：${graduateProjectFrom}
          <br>类别：${category}
          <br>年份：${year}
          <br>工作内容：${content}
          <br>基本需求：${requirements}
          <br>所需技能：${basicString}
          <br>参考文献：${reference}`
        })
      }
    }
  }, '显示')
};

// 显示工作进程按钮
const showScheduleButton = (vm, h, currentRow, index) => {
  return h('Button', {
    props: {
      type: 'primary',
      size: 'small'
    },
    style: {
      marginRight: '5px'
    },
    on: {
      click: () => {
        axios.get("/getStudent")
          .then(function (response) {
            if (response.status = 200) {
              //从后台得到学生全部信息
              var tableDataList = response.data.rows;
              //得到学生姓名
              var name = tableDataList[0].value;
              //得到班级
              var studentClass = tableDataList[0].studentClass.value;
              //得到指导教师
              var supervisor = tableDataList[0].supervisor.value;
              //得到题目
              var title = tableDataList[0].graduateProject.title;
              //var title= response.data.graduateProject.title;
              vm.$Modal.info({
                title: '显示详情',
                content: `姓名：${name}<br>班级：${studentClass}<br>指导教师：${supervisor}<br>题目：${title}<br>开始时间：${currentRow.startDate}<br>结束时间：${currentRow.endDate}<br>完成的工作内容：${currentRow.workToDo}<br>评语：${currentRow.comment}`
              })
            }
          })
      }
    }
  }, '显示')
};
const showProgressReportButton = (vm, h, currentRow, index) => {
  return h('Button', {
    props: {
      type: 'primary',
      size: 'small'
    },
    style: {
      marginRight: '5px'
    },
    on: {
      click: () => {
        axios.get("/getStudent")
          .then(function (response) {
            if (response.status = 200) {
              console.log("=====================" + response.data.rows + "测试")
              //从后台得到学生全部信息
              var tableDataList = response.data.rows;
              //得到学生姓名
              var name = tableDataList[0].value;
              //得到班级
              var studentClass = tableDataList[0].studentClass.value;
              //得到指导教师
              var supervisor = tableDataList[0].supervisor.value;
              //得到题目
              var title = tableDataList[0].graduateProject.title;
              //var title= response.data.graduateProject.title;
              vm.$Modal.info({
                title: '显示详情',
                content: `姓名：${name}<br>班级：${studentClass}<br>指导教师：${supervisor}<br>题目：${title}<br>上传时间：${currentRow.submitDate}<br>附件名：${currentRow.file}<br>评语：${currentRow.comment}`
              })
            }
          })
      }
    }
  }, '显示')
};
// 学生查看任务书页面显示任务书按钮
const showMissionDocButton = (vm, h, currentRow, index) => {
  return h('Button', {
    props: {
      type: 'primary',
      size: 'small'
    },
    style: {
      marginRight: '5px'
    },
    on: {
      click: () => {

        // 题目
        var title = currentRow.graduateProject.title;
        // 副标题
        var subTitle = currentRow.graduateProject.subTitle;
        // 题目类型
        var graduateProjectType = currentRow.graduateProject.graduateProjectType.value;
        // 题目来源
        var graduateProjectFrom = currentRow.graduateProject.graduateProjectFrom.value;
        // 题目性质
        var graduateProjectNature = currentRow.graduateProject.graduateProjectNature.value;
        // 年份
        var year = currentRow.graduateProject.year;
        // 工作内容
        var content = currentRow.graduateProject.content;
        // 基本需求
        var requirements = currentRow.graduateProject.requirements;
        // 所需技能
        var basicString = currentRow.graduateProject.basicString;
        // 参考文献
        var reference = currentRow.graduateProject.reference;
        // 学生姓名
        var student = currentRow.value;
        // 指导老师姓名
        var supervisor = currentRow.supervisor.value;
        // 指导老师学位
        var degree = currentRow.supervisor.degree.value;
        // 指导老师职称
        var profTitle = currentRow.supervisor.profTitle.value;
        // 类别
        var category = currentRow.graduateProject.category;
        // 班级
        var studentClass = currentRow.studentClass.value;
        // this.ifNull(student);
        vm.$Modal.info({
          title: '显示详情',
          content: `
          姓名：${student}
          <br>班级：${studentClass}
          <br>指导教师：${supervisor}
          <br>指导教师学位/职称：${degree}/${profTitle}
          <br>题目：${title}
          <br>副标题：${subTitle}
          <br>题目类型：${graduateProjectType}
          <br>题目性质：${graduateProjectNature}
          <br>题目来源：${graduateProjectFrom}
          <br>题目类别：${category}
          
          <br>工作内容：${content}
          <br>基本需求：${requirements}
          <br>所需技能：${basicString}
          <br>参考文献：${reference}`
        })
      }
    }
  }, '显示')
};
//学生查看任务书页面任务书状态
const showMissionDocState = (vm, h, currentRow, file) => {
  return h('span', {
    style: {
      color: 'blue',
      fontSize: '15px',
      fontWeight: 'bold',
    }
  }, '已下达任务书');
};
//学生查看任务书页面下载任务书
const downMissionDocHref = (vm,h,currentRow,file)=>{
  if(currentRow.graduateProject!=null&&currentRow.graduateProject.missionDoc.file!=null){
    let down = [downloadButton(vm,h,currentRow,currentRow.graduateProject.missionDoc.file)];//存放下载按钮的
      return h('div',down);
  }
};
//学生上传开题报告页面状态
const uploadOrNotHref = (vm,h,currentRow,file)=>{
  if(currentRow.file!=null){
    return h('span',{
      style:{
        color:'blue',
        fontSize:'15px',
        fontWeight:'bold',
      }
    },'已上传开题报告');
  }
  // else{
  //   h('span',{
  //     style:{
  //       color:'red',
  //       fontSize:'15px',
  //       fontWeight:'bold',
  //     }
  //   },'未上传开题报告');
  // }
}

//学生上传开题报告页面操作栏
const showOrDeleteOrDownHref = (vm,h,currentRow,file)=>{
  if(currentRow.file!=null){
    if(currentRow.approvalFromSupervisor.approvalState.value=="审核中"){
      return h('Button', {
        props: {
          type: 'primary',
          size: 'small'
        },
        style: {
          marginRight: '5px'
        },
        on: {
          click: () => {
           axios.get("/getStudent")
           .then(function(response){
               if(response.status = 200){
                 //从后台得到学生全部信息
                 var tableDataList = response.data.rows;
                 //得到学生姓名
                   var name = tableDataList[0].value;
                   //得到班级
                   var studentClass = tableDataList[0].studentClass.value;
                   //得到指导教师
                   var supervisor= tableDataList[0].supervisor.value;
                   //得到题目
                   var title = tableDataList[0].graduateProject.title;
                   //var title= response.data.graduateProject.title;
                    vm.$Modal.info({
                      title:'显示详情',
                      content:`姓名：${name}<br>班级：${studentClass}<br>指导教师：${supervisor}<br>题目：${title}<br>附件名：${currentRow.file}<br>指导老师审核：${currentRow.approvalFromSupervisor.approvalState.value}`
                    })
                }
           }.bind(this))
          }
        }
      }, '显示')
  }
  else if(currentRow.approvalFromSupervisor.approvalState.value=="审核成功"){
    let downAndDelete = [downloadButton(vm,h,currentRow,currentRow.file)];//存放下载按钮的
      return h('div',downAndDelete);
  }
  else{
    let downAndDelete = [deleteThesisProposalButton(vm,h,currentRow.thesisProposal,'/deleteProposerThesisProposal')];//存放删除按钮的
      return h('div',downAndDelete);
  }
}
};
//文件删除button
const deleteThesisProposalButton =(vm,h,currentRow,deleteUrl,index)=>{
  return h('Poptip', {   //iview的气泡提示
    props: {
      confirm: true,
      title: '您确定要删除已上传的文件吗?',
      transfer: true
    },
    on:{
      'on-ok': () => {
        axios({
          method:'delete',
          url:deleteUrl,
          data: currentRow
        })
          .then(function(response){
            if(response.status === 200){
              vm.tableDataList = response.data.rows;
              vm.pageTotals = response.data.total;
              vm.$Message.success({
                content:'删除成功',
                duration:5,
              });
              vm.$router.push('thesisProposalRouter');
            }else{
              vm.$Message.error({
                content:'删除失败',
                duration:5,
              });
            }
          })
          .catch(function(error){
            alert(error)
          });
      }
    }},[
    h('Button', {
      style: {
        margin: '0 2px'
      },
      props: {
        placement: 'top', //气泡弹出的位置
        type: 'error',
        icon: 'trash-a',
        size: 'small'
      },
      attrs: {
        title: '删除'
      }
    })
  ]);
};
//学生上传阶段成果页面下载阶段成果
const downTheisProposalHref = (vm,h,currentRow,file)=>{
  if(currentRow.file!=null){
    let down = [downloadButton(vm,h,currentRow,currentRow.thesisProposal.file)];//存放下载按钮的
      return h('div',down);
  }
};
// 分配学生分配按钮
const matchButton = (vm, h, currentRow, item) => {
  return h('Button', {
    props: {
      type: 'primary',
      size: 'small'
    },
    on: {
      click: () => {
        vm.$emit("showModal", currentRow);
      }
    }
  }, (item == 'match' ? '匹配' : '取消匹配'))
};
const showDetailButton = (vm, h, currentRow, item) => {
  return h('Button', {
    props: {
      type: 'primary',
      size: 'small'
    },
    style: {
      margin: '0 2px'
    },
    on: {
      click: () => {
        vm.$emit("showModal", currentRow);
      }
    }
  }, '显示详情')

}
const missionDocOperate = (vm, h, currentRow, index) => {
  if (currentRow.missionDoc === null) {
    return h(uploadbutton, {
      props: {
        headers: 'multipart/form-data;charset=UTF-8',
        buttonName: '上传任务书',
        url: '/bysj/uploadMissionDocFromTeacher?' + "graduateProjectId=" + currentRow.id + "&submit=" + true,
      },
      attrs: {
        id: 'upload'
      },
      on: {
        'init': () => {
           vm.$router.push('xdrwsRouter');
        }
      }
    });
  } else {
    let downAndDelete = [downloadButton(vm, h, currentRow, currentRow.missionDoc.file,), deleteFileButton(vm, h, currentRow.missionDoc, '/deleteProposerMissionDoc?graduateProjectId=' + currentRow.missionDoc.graduateProject + "&pageNum=" + vm.pageNum + "&pageSize=" + vm.pageSize)];
    return h('div', downAndDelete);
  }
};

//文件删除button
const deleteFileButton =(vm,h,currentRow,deleteUrl,index)=>{
  return h('Poptip', {   //iview的气泡提示
    props: {
      confirm: true,
      title: '您确定要删除已上传的文件吗?',
      transfer: true
    },
    on:{
      'on-ok': () => {
        axios({
          method:'delete',
          url:deleteUrl,
          data: currentRow
        })
          .then(function(response){
            if(response.status === 200){
              vm.tableDataList = response.data.rows;
              vm.pageTotals = response.data.total;
              vm.$Message.success({
                content:'删除成功',
                duration:5,
              });
              vm.$router.push('xdrwsRouter');
            }else{
              vm.$Message.error({
                content:'删除失败',
                duration:5,
              });
            }
          })
          .catch(function(error){
            alert(error)
          });
      }
    }},[
    h('Button', {
      style: {
        margin: '0 2px'
      },
      props: {
        placement: 'top', //气泡弹出的位置
        type: 'error',
        icon: 'trash-a',
        size: 'small'
      },
      attrs: {
        title: '删除'
      }
    })
  ]);
};
const downloadButton = (vm, h, currentRow, fileName) => {
  return h('a', {
    attrs: {
      href: fileName,
      download: "",
    },
    style: {
      color: '#fff',
      backgroundColor: '#2d8cf0',
      borderColor: '#2d8cf0',
      padding: '2px 7px',
      fontSize: ' 16px',
      borderradius: '3px',
      outline: 0,
      margin: '0 2px'
    },
  }, (fileName == null || fileName == '') ? '未上传' : '下载');
};
//文件下载a标签
const downHref = (vm, h, currentRow, fileName) => {
  return h('a', {
    attrs: {
      href: fileName,
      download: fileName,
    },
  }, (fileName != null) ? (currentRow.evaluationReport == null) ? '未评审' : '下载' : '未上传');
};

//审核通过与未通过使用到的开关
const rowSwitch = (vm, h, currentRow, index) => {
  if (currentRow.student != null) {
    if (currentRow.thesisProposal != null) {
      let switchValue = 0;
      if (currentRow.thesisProposal.approvalFromSupervisor != null) {
        switchValue = currentRow.thesisProposal.approvalFromSupervisor.approvalState.id;
      }
      return h('i-switch', {
        props: {
          size: 'large',
          type: 'primary',
          value: switchValue === 2,
        },
        style: {
          marginRight: '5px'
        },
        on: {
          'on-change': (value) => {
            let approvalId;
            if (value) {
              approvalId = 2;//审核成功
            } else {
              approvalId = 3;
            }
            axios.put('/approvalThesisProposal?approvalId=' + approvalId + '&thesisGraduateProjectId=' + currentRow.id);
          }
        }
      }, [
        h('span', {slot: 'open'}, '通过'),
        h('span', {slot: 'close'}, '驳回')
      ]);
    } else {
      return h('span', {
        style: {
          color: 'red',
          fontSize: '16px',
          fontWeight: 'bold',
        }
      }, '学生未上传开题报告');
    }
  } else {
    return h('span', {
      style: {
        color: 'red',
        fontSize: '16px',
        fontWeight: 'bold',
      }
    }, '请先进行分配题目');
  }

};
//行--编辑/保存][按钮
const editButton = (vm, h, currentRow, index) => {
  return h('Button', {
    props: {
      type: 'primary',
      size: 'small',
      icon: currentRow.editting ? 'checkmark' : 'ios-compose'  //平时状态是编辑按钮。编辑状态变成保存按钮
    },
    attrs: {
      title: currentRow.editting ? '保存' : '编辑'
    },
    style: {
      margin: '0 2px'
    },
    on: {
      'click': () => {  //这是点击引发的事件. editting为true时框框打开编辑中,为false时,编辑结束.关闭了框框.
        if (!currentRow.editting) {  //editting为false的时候,显示的是编辑按钮.此时是没有打开框框的.也就是说这是点击编辑按钮引发的事件
          if (currentRow.edittingCell) { //如果该行有可编辑的列.
            for (let cellName in currentRow.edittingCell) {
              currentRow.edittingCell[cellName] = false;
              vm.thisTableData[index].edittingCell[cellName] = false;
            }
          }
          vm.thisTableData[index].editting = true;  //点击编辑按钮之后，这行数据的编辑状态变为true
        } else {  //editting为true的时候,显示的是保存按钮. 此时打开了框框.也就是说这是点击保存按钮引发的事件
          if (currentRow.adding) {
            delete vm.thisTableData[0].adding; //删掉adding属性
            vm.thisTableData[index].editting = false;  //点击保存按钮之后, 这行数据的编辑状态变为false
            let edittingRow = vm.thisTableData[index]; //当前正在编辑的该行的数据
            //父组件@on-change的时候，可以用这两个参数
            vm.$emit('on-change', vm.handleObjectDataToBackData(edittingRow));  //应该由父组件做这件事  因为每个父组件传的url是不一样的
          } else {
            vm.thisTableData[index].editting = false;  //点击保存按钮之后, 这行数据的编辑状态变为false
            vm.thisTableData = JSON.parse(JSON.stringify(vm.thisTableData));//因为修改的数据保存在edittingStore里面呢,所以得重新赋值一下,然后才能得到当前最新数据,传给后台

            let edittingRow = vm.thisTableData[index]; //当前正在编辑的该行的数据
            //父组件@on-change的时候，可以用这两个参数
            vm.$emit('on-change', vm.handleObjectDataToBackData(edittingRow));  //应该由父组件做这件事  因为每个父组件传的url是不一样的
          }
        }
      }
    }
  });
};

//行--删除/取消按钮
const deleteButton = (vm, h, currentRow, index) => {
  if (currentRow.editting) {  //editting为true的时候,显示的是取消按钮. 此时打开了框框.
    return h('Button', {
      props: {
        type: 'warning',
        size: 'small',
        icon: 'close'
      },
      attrs: {
        title: '取消'
      },
      style: {
        margin: '0 2px'
      },
      on: {
        'click': () => {  //这是点击编辑的取消按钮引发的事件
          if (currentRow.adding) {
            vm.value.splice(0, 1);
            vm.thisTableData.splice(0, 1); //移除
          } else {
            vm.thisTableData[index].editting = false;  //点击取消按钮之后，这行数据的保存状态才是false
          }
        }
      }
    });
  } else {
    return h('Poptip', {   //iview的气泡提示
      props: {
        confirm: true,
        title: '您确定要删除这条数据吗?',
        transfer: true
      },
      on: {  //点击ok触发删除
        'on-ok': () => {
          let deleteRow = vm.thisTableData[index]; //当前正在编辑的该行的数据
          vm.$emit('on-delete', vm.handleObjectDataToBackData(deleteRow));
        }
      }
    }, [
      h('Button', {
        style: {
          margin: '0 2px'
        },
        props: {
          placement: 'top', //气泡弹出的位置
          type: 'error',
          icon: 'trash-a',
          size: 'small'
        },
        attrs: {
          title: '删除'
        }
      })
    ]);
  }

};

//列编辑按钮
const incellEditBtn = (vm, h, param) => {
  return h('Button', {
    props: {
      type: 'text',
      icon: 'edit'
    },
    on: {
      click: (event) => {
        vm.thisTableData[param.index].edittingCell[param.column.key] = true;
      }
    }
  });
};

//列保存按钮
const saveIncellEditBtn = (vm, h, param) => {
  return h('Poptip', {
    props: {
      confirm: true,
      title: '您确定编辑该列数据吗?',
      transfer: true
    },
    on: {
      'on-ok': () => {
        vm.thisTableData[param.index].edittingCell[param.column.key] = false;
        let edittingRow = vm.thisTableData[param.index];
        vm.$emit('on-change', vm.handleObjectDataToBackData(edittingRow));
      },
      'on-cancel': () => {
        vm.thisTableData[param.index].edittingCell[param.column.key] = false;
      }
    }
  }, [
    h('Button', {
      props: {
        placement: 'top',
        type: 'text',
        icon: 'checkmark'
      },
    })
  ]);
};

//重置密码btn
const resetButton = (vm, h, currentRow, index) => {
  //编辑状态（增加、行编辑）隐藏 此按钮
  if (!currentRow.editting) {
    return h('Poptip', {   //iview的气泡提示
      props: {
        confirm: true,
        title: '您确定要重置密码吗？',
        transfer: true
      },
      on: {  //点击ok触发删除
        'on-ok': () => {
          let resetRow = vm.thisTableData[index]; //当前正在编辑的该行的数据
          vm.$emit('on-reset', vm.handleObjectDataToBackData(resetRow));
        }
      }
    }, [h('Button', {
      style: {
        margin: '0 2px'
      },
      props: {
        placement: 'top', //气泡弹出的位置
        type: 'warning',
        icon: 'android-refresh',
        size: 'small'
      },
      attrs: {
        title: '重置密码'
      }
    })
    ]);
  }
};

//重置密码操作
const axiosResetPassword = (url, currentRow, vm) => {
  axios.put(url, currentRow.id)
    .then(function (res) {
      if (res.status == 200) {
        vm.$Message.success({
          content: '密码重置成功',
          duration: 5,
        });
      }
    });
};

const viewBasicPage = (vm, h, currentRowData, title, emitName) => {
  if (!currentRowData.editting) {
    return h("Button", {
      style: {
        margin: "0 2px",
        padding: "2px 6px"
      },
      props: {
        type: "success",
        icon: "eye",
        size: 'small'
      },
      attrs: {
        title: title
      },
      on: {
        "click": () => {
          vm.$emit(emitName, currentRowData.id);
        }
      }
    });
  }
};

const settingRole = (vm, h, currentRow) => {
  return h("Button", {
    style: {
      margin: "0 2px",
      padding: "2px 6px"
    },
    props: {
      type: "warning",
      icon: "gear-b",
      size: 'small'
    },
    attrs: {
      title: "设置用户角色"
    },
    on: {
      "click": () => {

        if (currentRow.user == null) {
          vm.$Message.warning("没有用户信息");
          return;
        }

        axios.all([axios.get("/getAllRole"), axios.get("/getRoleByUserId?id=" + currentRow.user.id)])
          .then(axios.spread(function (acct, perms) {
            var ids = [];
            perms.data.forEach((item) => {
              ids.push(item.role.key + "");
            });

            vm.$emit("settingRole", currentRow.user.id, acct.data, ids);

          }));
      }
    }
  });
};

const addUserRole = (id, keys) => {

  axios({
    method: 'post',
    url: "/addUserRole?userId=" + id + "&roleIds=" + keys.join(","),
  }).then(function (res) {

  });

};

const deleteUserRole = (id, roleIds) => {
  axios.delete("/deleteUserRow?userId=" + id + "&roleIds=" + roleIds.join(","));
};
const handleOrExport = (vm, h, currentRow, item, title) => {
  console.log(currentRow[item.key]);
  if (currentRow[item.key] == null) {
    return h('Button', {
      style: {
        margin: "0 2px",
        padding: "2px 6px"
      },
      props: {
        type: 'info',
        icon: "ios-paper",
        size: 'small'
      },
      attrs: {
        title: title
      },
      on: {
        "click": () => {
          vm.$emit("handleOrExport", currentRow.id);
        }
      }
    });
  } else {
    switch (item.key) {
      case "reviewReport":
        return h('a', {
          attrs: {
            href: "/bysj/exportReviewReport?id=" + currentRow['id'],
            title: "导出"
          }
        }, '导出');
      case "questionerTeamReport":
        return h('a', {
          attrs: {
            href: "/bysj/exportQuestionerTeamReport?id=" + currentRow['id'],
            title: "导出"
          }
        }, '导出');
    }
  }
};

const checkButton = (vm, h, currentRow, index) => {
  return h('Button', {
    props: {
      type: 'ghost',
      size: 'small',
      icon: "coffee"
    },
    on: {
      click: () => {
        vm.$emit("showModal", currentRow);
      }
    }
  }, "审核")
};
const reviewButton = (vm, h, currentRow, item) => {
  if(currentRow[item.key]==null){
    return h('Button', {
      props: {
        type: 'ghost',
        size: 'small',
        icon: "coffee"
      },
      on: {
        click: () => {
          vm.$emit("showModal", currentRow);
        }
      }
    }, "评审")
  }else{
    return h('a', {
      attrs: {
        href: "/bysj/exportEvaluationReport?id=" + currentRow.id,
        title: "导出"
      }
    }, '导出');
  }
};
const tableCRUDButton = {
  axiosGet: axiosGet,
  axiosPut: axiosPut,
  axiosDelete: axiosDelete,
  selectUrl: selectUrl,
  editButton: editButton,
  deleteButton: deleteButton,
  incellEditBtn: incellEditBtn,
  saveIncellEditBtn: saveIncellEditBtn,
  matchButton: matchButton,
  showButton: showButton,
  showStudentsFromTeacherButton: showStudentsFromTeacherButton,
  showScheduleButton:showScheduleButton,
  showProgressReportButton:showProgressReportButton,
  showMissionDocButton:showMissionDocButton,
  showMissionDocState:showMissionDocState,
  uploadOrNotHref:uploadOrNotHref,
  showOrDeleteOrDownHref:showOrDeleteOrDownHref,
  showScheduleButton:showScheduleButton,
  showDetailButton:showDetailButton,
  missionDocOperate: missionDocOperate,
  resetButton: resetButton,
  axiosResetPassword: axiosResetPassword,
  viewBasicPage: viewBasicPage,
  settingRole: settingRole,
  addUserRole: addUserRole,
  deleteUserRole: deleteUserRole,
  handleOrExport: handleOrExport,
  reviewButton: reviewButton,
  downloadButton: downloadButton,
  rowSwitch: rowSwitch,
  downHref: downHref,
  downMissionDocHref: downMissionDocHref,
  checkButton: checkButton,
};

export default tableCRUDButton;
