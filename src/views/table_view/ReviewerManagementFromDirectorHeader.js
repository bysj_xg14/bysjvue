
export const ReviewTopicHeader = [
    {
        title: '序号',
        type: 'index',   //列类型
        align: 'center' //字体居中
    },
    {
        width: 1,
        align: 'center',
        key: 'id',
        render: (h, params) => {
          return h('div', {
            style: {
              display: false
            }
          });
        },
        className: 'hidden'
    }, 
	{
		title: '题目名称',
		key: 'title',
		align: 'center',
		editable: false,
		sortable: false,
		cellType: ''
	},
    {
        title: '类别',
        key: 'category',
        align: 'center',
        editable: false,
        sortable: false,
        cellType: ''
    },
    {
        title: '类型',
        key: 'graduateProjectType',
        align: 'center',
        editable: false,
        sortable: false,
        cellType: 'object'
    },
    {
        title: '性质',
        key: 'graduateProjectNature',
        align: 'center',
        editable: false,
        sortable: false,
        cellType: 'object'
    },
    {
        title: '年份',
        key: 'year',
        align: 'center',
        editable: false,
        sortable: false,
        cellType: ''
    },
	{
		title: '指定评阅人',
		key: 'reviewer',
		align: 'center',
		editable: true,
		sortable: false,
		cellType: 'object_edit'
	},
     {
        title: '详情',
        align: 'center',
        key: 'handle',  // 定义handle关键字
        handle: ['show']
    }
	
];


export default ReviewTopicHeader;