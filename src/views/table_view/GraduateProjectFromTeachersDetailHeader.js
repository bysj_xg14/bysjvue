export const GraduateProjectFromTeachersDetailHeader = [
   {
		width: 1,
        align: 'center',
		key: 'id',
		render: (h, params) => {
            return h('div', {
                    style: {
                       display: false
                    }
                });
		},
		className: 'hidden'
	},
	{   width:300,
		title: '课题名称',
		key: 'title',
		align: 'center',
		sortable: false,
		cellType: '',
	},
	{   
		title: '指导老师',
		key: 'proposer',
		align: 'center',
		sortable: false,
		cellType: 'object',
	},
	{   
		title: '题目性质',
		key: 'graduateProjectNature',
		align: 'center',
		sortable: false,
		cellType: 'object',
	},
	{   
		title: '题目来源',
		key: 'graduateProjectFrom',
		align: 'center',
		sortable: false,
		cellType: 'object',
	},
	{   
		title: '题目类型',
		key: 'graduateProjectType',
		align: 'center',
		sortable: false,
		cellType: 'object',
	},
	{   
		title: '工作内容',
		key: 'content',
		align: 'center',
		sortable: false,
		cellType: 'textarea',
	},
	{   
		title: '基本需求',
		key: 'requirements',
		align: 'center',
		sortable: false,
		cellType: 'textarea',
	},
	{  
		title: '所需技能',
		key: 'basicString',
		align: 'center',
		sortable: false,
		cellType: 'textarea',
	},
	
	{   
		title: '参考文献',
		key: 'reference',
		align: 'center',
		sortable: false,
		cellType: 'textarea',
	},
];


export default GraduateProjectFromTeachersDetailHeader;