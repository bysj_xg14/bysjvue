export const GraduateProApplicationsHeader = [
    {
        type: 'selection',
        width: 60,
        align: 'center'
    },
    {
        title: '序号',
        type: 'index',   //列类型
        align: 'center' //字体居中
    },
    {
		width: 1,
        align: 'center',
		key: 'id',
		render: (h, params) => {
            return h('div', {
                    style: {
                       display: false
                    }
                });
        }
    },
	{
		title: '填报志愿的学生',
		key: 'actor',
		align: 'center',
		editable: true,
		sortable: false,
		cellType: '',
	},
	{
		title: '自我介绍',
		key: 'selfIntroduction',
		align: 'center',
		editable: true,
		sortable: false,
		cellType: '',
	},
	{
		title: '是否被录取',
		key: 'accepted',
		align: 'center',
		editable: true,
		sortable: false,
		cellType: '',
	},
	{
		title: '等级',
		key: 'rank',
		align: 'center',
		editable: true,
		sortable: false,
		cellType: '',
	},

    {
        title: '操作',
        align: 'center',
        key: 'handle',  // 定义handle关键字
        handle: ['edit', 'delete']
    }
];


export default GraduateProApplicationsHeader;