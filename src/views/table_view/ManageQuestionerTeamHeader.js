export const QuestionerTeamHeader = [
  {
    type: 'selection',
    width: 40,
    align: 'center',
    className: "ive-table-th-checkbox"
  },
  {
    title: '序号',
    width: 60,
    type: 'index',   //列类型
    align: 'center', //字体居中
    className: "ive-table-th"
  },
  {
    width: 1,
    align: 'center',
    key: 'id',
    className: 'hidden'
  },
  {
    title: '答辩组长',
    width: 100,
    key: 'leader.value',
    align: 'center',
    editable: false,
    sortable: false,
    cellType: 'object_common',
  },
  {
    title: '答辩老师',
    key: 'supervisors.value',
    align: 'center',
    editable: false,
    sortable: false,
    cellType: 'object_common_multi',
  },
  {
    title: '答辩学生',
    key: 'students.value',
    align: 'center',
    editable: false,
    sortable: false,
    cellType: 'object_common_multi',
  },
  {
    title: '开始时间',
    width: 140,
    key: 'startTime',
    align: 'center',
    editable: true,
    sortable: false,
    cellType: 'datePicker',
  },
  {
    title: '结束时间',
    width: 140,
    key: 'endTime',
    align: 'center',
    editable: true,
    sortable: false,
    cellType: 'datePicker',
  },
  {
    title: '答辩地点',
    key: 'address',
    align: 'center',
    editable: true,
    sortable: false,
    cellType: '',
  },
  {
    title: '操作',
    align: 'center',
    key: 'handle',  // 定义handle关键字
    handle: ['edit', 'delete']
  }
];


export default QuestionerTeamHeader;
