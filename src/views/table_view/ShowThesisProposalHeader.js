//只有论文的有开题报告
export const ShowThesisProposalHeader = [
    {
		width: 1,
        align: 'center',
		key: 'id',
		render: (h, params) => {
            return h('div', {
                    style: {
                       display: false
                    }
                });
		},
		className: 'hidden'
	},
	{
		title: '主指导审核状态',
		key: 'approvalFromSupervisor.approvalState.value',
		align: 'center',
		editable: false,
		sortable: false,
		cellType: 'object_common',
	},
	{
		title: '题目名称',
		key: 'thesisGraduateProject.title',
		align: 'center',
		editable: false,
		sortable: false,
		cellType: 'object_common',
	},
	{   width: 150,
		title: '副标题',
		key: 'thesisGraduateProject.subTitle',
		align: 'center',
		editable: false,
		sortable: false,
		cellType: 'object_common',
	},
	{
		title: '姓名',
		key: 'thesisGraduateProject.student.value',
		align: 'center',
		sortable: false,
		cellType: 'object_common',
	},
	{
		title: '学号',
		key: 'thesisGraduateProject.student.idNo',
		align: 'center',
		sortable: false,
		cellType: 'object_common',
	},
	{
		title: '查看开题报告',
		key: 'file',
		align: 'center',
		cellType:'download',//下载学生上传的开题报告
	},
];


export default ShowThesisProposalHeader;