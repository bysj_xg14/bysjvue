export const StudentDistributionOfStudentsHeader = [
    {
        title: '选择',
        type: 'selection',
        width: 60,
        align: 'center'
    },
    {
        width: 1,
        align: 'center',
        key: 'id',
        render: (h, params) => {
            return h('div', {
                    style: {
                       display: false
                    }
                });
        }
    },
    {
        title: '班级',
        key: 'studentClass',
        align: 'center',
        editable: true,
        sortable: false,
        cellType: 'object'
    },
    {
        title: '姓名',
        key: 'name',
        align: 'center',
        editable: true,
        sortable: false,
        cellType: ''
    },
    {
        title: '学号',
        key: 'idNo',
        align: 'center',
        editable: true,
        sortable: false,
        cellType: ''
    }

    
];


export default StudentDistributionOfStudentsHeader;