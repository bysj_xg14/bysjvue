// 指导老师审阅阶段成果表Modal的表头
export const ProgressReportDetailHeader = [
    {
		width: 1,
        align: 'center',
		key: 'id',
		render: (h, params) => {
            return h('div', {
                    style: {
                       display: false
                    }
                });
		},
		className: 'hidden'
	},
	{
		title: '序号',
		width: 60,
		type: 'index',   //列类型
		align: 'center', //字体居中
		className: "ive-table-th"
	  },
	{   
		title: '上传时间',
		key: 'submitDate',
		align: 'center',
		sortable: false,
		cellType:'',
	},
	{  
		title: '评语',
		key: 'comment',
		align: 'center',
		editable: true,
		sortable: false,
		cellType:'',
	},
	{
		title: '操作',
		key: 'file',
		align: 'center',
		editable: false,
		sortable: false,
		cellType: 'download',
	  }
];
export default ProgressReportDetailHeader;