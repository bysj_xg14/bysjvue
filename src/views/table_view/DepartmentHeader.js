export const DepartmentHeader = [
  {
    type: 'selection',
    width: 40,
    align: 'center',
    className: "ive-table-th-checkbox"
  },
  {
    width: 1,
    align: 'center',
    key: 'id',
    className: "ive-table-th"
  },
  {
    title: '编号',
    key: 'no',
    align: 'center',
    editable: true,
    sortable: false,
    cellType: '',
  },
  {
    title: '教研室',
    key: 'value',
    align: 'center',
    editable: true,
    sortable: false,
    cellType: '',
  },
  {
    title: '学院',
    key: 'school',
    align: 'center',
    editable: true,
    sortable: false,
    cellType: 'object',
  },

  {
    title: '操作',
    align: 'center',
    key: 'handle',  // 定义handle关键字
    handle: ['edit', 'delete', "viewMajor"]
  }
];


export default DepartmentHeader;
