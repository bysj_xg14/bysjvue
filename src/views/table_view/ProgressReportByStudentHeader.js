export const ProgressReportByStudentHeader = [
    {
        title: '序号',
        type: 'index',   //列类型
		align: 'center', //字体居中
		width:100,
	},
	//隐藏id
    {
		width: 1,
        align: 'center',
		key: 'id',
		render: (h, params) => {
            return h('div', {
                    style: {
                       display: false
                    }
                });
		},
		className:'hidden'
    },

	{
		title: '上传时间',
		key: 'submitDate',
		align: 'center',
		editable: false,
		sortable: true,
		cellType: 'datePicker',
	},
	
	{
		title: '附件名',
		key: 'file',
		align: 'center',
		width:250,
		editable: false,
		sortable: false,
		cellType: 'file_name',
	},
	
	{
		title: '评语',
		key: 'comment',
		align: 'center',
		editable: false,
		sortable: false,
		cellType: '',
	},
	
    {
        title: '操作',
		align: 'center',
		width:100,
        key: 'handle',  // 定义handle关键字
        handle: ['showProgressReport']
    }
];


export default ProgressReportByStudentHeader;