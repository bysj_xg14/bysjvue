export const GraduateProjectFromTeachersHeader = [

    {   width: 150,
        title: '序号',
        type: 'index',   //列类型
		align: 'center', //字体居中
	},
   {
		width: 1,
        align: 'center',
		key: 'id',
		render: (h, params) => {
            return h('div', {
                    style: {
                       display: false
                    }
                });
		},
		className: 'hidden'
	},
	{  
		title: '提交状态',
		key: 'submited',
        align: 'center',
		sortable: false,
		cellType: 'submit_show',
	},
	{   width:300,
		title: '课题名称',
		key: 'title',
		align: 'center',
		sortable: false,
		cellType: '',
	},
	{  
		title: '副标题',
		key: 'subTitle',
		align: 'center',
		sortable: false,
		cellType: '',
	},
	{   
		title: '年份',
		key: 'year',
		align: 'center',
		sortable: false,
		cellType: '',
	},
	{
		title:'题目类别',
        key:'category', 
		align:'center',
		cellType:'',
	},
	{//获取来自教研室主任的审核的状态
		title: '审核状态',
		key: 'approvalFromDirector.approvalState.value',
		align: 'center',
		sortable: false,
		cellType: 'object_common',
	},
	{
        title: '操作',
        align: 'center',
        key: 'handle',  // 定义handle关键字
        handle: ['showDetail']
    }
	
];


export default GraduateProjectFromTeachersHeader;