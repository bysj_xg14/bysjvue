export const VisitingEmployeeHeader = [
  {
    type: 'selection',
    width: 40,
    align: 'center',
    className: "ive-table-th-checkbox"
  },
  {
    title: '序号',
    width: 60,
    type: 'index',   //列类型
    align: 'center', //字体居中
    className: "ive-table-th"
  },
  {
    width: 1,
    align: 'center',
    key: 'id',
    render: (h, params) => {
      return h('div', {
        style: {
          display: false
        }
      });
    },
    className: 'hidden'
  },
  {
    title: '姓名',
    width: 100,
    key: 'value',
    align: 'center',
    editable: true,
    sortable: false,
    cellType: '',
    className: "ive-table-th"
  },
  {
    title: '职称',
    width: 100,
    key: 'profTitle',
    align: 'center',
    editable: true,
    sortable: false,
    cellType: 'object',
    className: "ive-table-th"
  },
  {
    title: '学位',
    width: 100,
    key: 'degree',
    align: 'center',
    editable: true,
    sortable: false,
    cellType: 'object',
    className: "ive-table-th"
  },
  {
    title: '电话',
    width: 150,
    key: 'mobile',
    align: 'center',
    editable: true,
    sortable: false,
    cellType: '',
    className: "ive-table-th"
  },
  {
    title: '受聘教研室',
    width: 150,
    key: 'department',
    align: 'center',
    editable: true,
    sortable: false,
    cellType: 'object',
    className: "ive-table-th"
  },
  {
    title: '职位',
    key: 'position',
    align: 'center',
    editable: true,
    sortable: false,
    cellType: '',
    className: "ive-table-th"
  },
  {
    title: '所属公司',
    width: 100,
    ellipsis: true,
    key: 'company',
    align: 'center',
    editable: true,
    sortable: false,
    cellType: '',
    className: "ive-table-th"
  },

  {
    title: '操作',
    align: 'center',
    width: 150,
    key: 'handle',  // 定义handle关键字
    handle: ['edit', 'delete', "reset"]
  }
];


export default VisitingEmployeeHeader;
