export const VisitingTeacherDisributionOfStudentHeader = [
	{
	    width: 1,
	    align: 'center',
	    key: 'id',
	    render: (h, params) => {
	      return h('div', {
	        style: {
	          display: false
	        }
	      });
	    },
	    className: 'hidden'
    }, 
    {
		title: '指导老师',
		key: 'value',
		align: 'center',
		editable: false,
		sortable: false,
		cellType: ''
	},
	{
		title: '职称',
		key: 'profTitle',
		align: 'center',
		editable: false,
		sortable: false,
		cellType: 'object'
	},
	{
		title: '已分配学生',
		key: 'students',
		align: 'center',
		editable: false,
		sortable: false,
		cellType: 'object_array_student'
	},
	{
		title: '匹配学生',
		key: 'studentsDistribution',
		align: 'center',
		editable: true,
		sortable: false,
		cellType: 'mobject_queaitonTeam'
	},
    // {
    //     title: '操作',
    //     align: 'center',
    //     key: 'handle',  // 定义handle关键字
    //     handle: ['edit', 'delete']
    // }
];


export default VisitingTeacherDisributionOfStudentHeader;