export const ProfTitleHeader = [
  {
    title: '序号',
    width: 60,
    type: 'index',   //列类型
    align: 'center', //字体居中
    className: "ive-table-th"
  },
  {
    width: 1,
    align: 'center',
    key: 'id',
    className: 'hidden'
  },
  {
    title: '教师姓名',
    key: 'value',
    align: 'center',
    editable: false,
    sortable: false,
    cellType: '',
  },
  {
    title: '职工号',
    key: 'staffNum',
    align: 'center',
    editable: false,
    sortable: true,
    cellType: '',
  },
  {
    title: '现有角色',
    key: 'user',
    align: 'center',
    editable: false,
    sortable: false,
    cellType: 'object_role',
  },
  {
    title: '操作',
    width: 150,
    align: 'center',
    key: 'handle',  // 定义handle关键字
    handle: ['settingRole']
  }
];


export default ProfTitleHeader;
