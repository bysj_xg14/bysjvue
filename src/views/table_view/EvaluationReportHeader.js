export const EvaluationReportHeader = [
    {
        type: 'selection',
        width: 60,
        align: 'center'
    },
    {
        title: '序号',
        type: 'index',   //列类型
        align: 'center' //字体居中
    },
    {
		width: 1,
        align: 'center',
		key: 'id',
		render: (h, params) => {
            return h('div', {
                    style: {
                       display: false
                    }
                });
        }
    },
	{
		title: '课题',
		key: 'graduateProject',
		align: 'center',
		editable: true,
		sortable: false,
		cellType: 'object',
	},
	{
		title: '评语',
		key: 'comment',
		align: 'center',
		editable: true,
		sortable: false,
		cellType: '',
	},
	{
		title: '中英文摘要（有无）',
		key: 'summary',
		align: 'center',
		editable: true,
		sortable: false,
		cellType: '',
	},
	{
		title: '外文资料翻译（有无）',
		key: 'docTranslation',
		align: 'center',
		editable: true,
		sortable: false,
		cellType: '',
	},
	{
		title: '论文字数',
		key: 'numberOfPapers',
		align: 'center',
		editable: true,
		sortable: false,
		cellType: '',
	},
	{
		title: '完成任务情况及水平',
		key: 'level',
		align: 'center',
		editable: true,
		sortable: false,
		cellType: '',
	},
	{
		title: '独立工作能力、分析与解决问题的能力',
		key: 'workAbiliity',
		align: 'center',
		editable: true,
		sortable: false,
		cellType: '',
	},
	{
		title: '工作量、工作态度',
		key: 'workload',
		align: 'center',
		editable: true,
		sortable: false,
		cellType: '',
	},
	{
		title: '基本理论、基本知识、基本技能和外语水平',
		key: 'basicTheory',
		align: 'center',
		editable: true,
		sortable: false,
		cellType: '',
	},
	{
		title: '外文资料翻译字数',
		key: 'numOfDocTranslation',
		align: 'center',
		editable: true,
		sortable: false,
		cellType: '',
	},
	{
		title: '中期检查报告',
		key: 'interimReport',
		align: 'center',
		editable: true,
		sortable: false,
		cellType: '',
	},
	{
		title: '任务完成情况',
		key: 'missionCompleted',
		align: 'center',
		editable: true,
		sortable: false,
		cellType: '',
	},

    {
        title: '操作',
        align: 'center',
        key: 'handle',  // 定义handle关键字
        handle: ['edit', 'delete']
    }
];


export default EvaluationReportHeader;