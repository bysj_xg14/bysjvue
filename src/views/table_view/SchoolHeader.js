export const DesignGraduateProjectFromStudentHeader = [
  {
    type: 'selection',
    width: 60,
    align: 'center'
  },
  {
    width: 1,
    align: 'center',
    key: 'id',
    render: (h, params) => {
      return h('div', {
        style: {
          display: false
        }
      });
    },
    className: 'hidden'
  },
  {
    title: '编号',
    key: 'no',
    width: 100,
    align: 'center',
    editable: true,
    sortable: false,
    cellType: '',
  },
  {
    title: '学院',
    key: 'value',
    align: 'center',
    editable: true,
    sortable: false,
    cellType: '',
  },

  {
    title: '操作',
    align: 'center',
    width: 150,
    key: 'handle',  // 定义handle关键字
    handle: ['edit', 'delete', "viewDepartment"]
  }
];


export default DesignGraduateProjectFromStudentHeader;
