export const RemarkTemplateForDesignGroupHeader = [
    {
        type: 'selection',
        width: 60,
        align: 'center'
    },
    {
        title: '序号',
        type: 'index',   //列类型
        align: 'center' //字体居中
    },
    {
		width: 1,
        align: 'center',
		key: 'id',
		render: (h, params) => {
            return h('div', {
                    style: {
                       display: false
                    }
                });
        }
    },
	{
		title: '教研室',
		key: 'department',
		align: 'center',
		editable: true,
		sortable: false,
		cellType: 'object',
	},
	{
		title: '指导老师',
		key: 'supervisor',
		align: 'center',
		editable: true,
		sortable: false,
		cellType: '',
	},
	{
		title: '是否是默认模板',
		key: 'defaultRemarkTemplate',
		align: 'center',
		editable: true,
		sortable: false,
		cellType: '',
	},
	{
		title: '评语类型',
		key: 'category',
		align: 'center',
		editable: true,
		sortable: false,
		cellType: '',
	},
	{
		title: '评语名称',
		key: 'title',
		align: 'center',
		editable: true,
		sortable: false,
		cellType: '',
	},
    {
        title: '操作',
        align: 'center',
        key: 'handle',  // 定义handle关键字
        handle: ['edit', 'delete']
    }
];


export default RemarkTemplateForDesignGroupHeader;