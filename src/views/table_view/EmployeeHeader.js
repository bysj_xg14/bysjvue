export const EmployeeHeader = [
  {
    type: 'selection',
    width: 40,
    align: 'center',
    className: "ive-table-th-checkbox"
  },
  {
    title: '序号',
    width: 60,
    type: 'index',   //列类型
    align: 'center', //字体居中
    className: "ive-table-th"
  },
  {
    width: 1,
    align: 'center',
    key: 'id',
    className: 'hidden'
  },
  {
    title: '姓名',
    width: 100,
    key: 'value',
    align: 'center',
    editable: true,
    sortable: false,
    cellType: '',
    className: "ive-table-th"
  },
  {
    title: '职工号',
    key: 'staffNum',
    align: 'center',
    editable: true,
    sortable: true,
    cellType: '',
  },
  {
    title: '职称',
    width: 100,
    key: 'profTitle',
    align: 'center',
    editable: true,
    sortable: false,
    cellType: 'object',
    className: "ive-table-th"
  },
  {
    title: '学位',
    width: 100,
    key: 'degree',
    align: 'center',
    editable: true,
    sortable: false,
    cellType: 'object',
    className: "ive-table-th"
  },
  {
    title: '电话',
    key: 'mobile',
    align: 'center',
    editable: true,
    sortable: false,
    cellType: '',
  },
  {
    title: '教研室',
    key: 'department',
    align: 'center',
    editable: true,
    sortable: false,
    cellType: 'object',
  },


  {
    title: '操作',
    width: 150,
    align: 'center',
    key: 'handle',  // 定义handle关键字
    handle: ['edit', 'delete', 'reset']
  }
];


export default EmployeeHeader;
