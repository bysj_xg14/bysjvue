export const EmployeeDistributionOfStudentHeader = [
	{
	    width: 1,
	    align: 'center',
	    key: 'id',
	    render: (h, params) => {
	      return h('div', {
	        style: {
	          display: false
	        }
	      });
	    },
	    className: 'hidden'
    }, 
    {
		title: '指导老师',
		key: 'value',
		align: 'center',
		editable: false,
		sortable: false,
		cellType: ''
	},
	{
		title: '职称',
		key: 'profTitle',
		align: 'center',
		editable: false,
		sortable: false,
		cellType: 'object'
	},
	{
        title: '显示已经匹配的学生',
        align: 'center',
        key: 'handle',  // 定义handle关键字
        handle: ['showStudentsFromTeacher']
    },

	{
		title: '匹配学生',
		key: 'studentsDistribution',
		align: 'center',
		editable: true,
		sortable: false,
		cellType: 'mobject'
	},
];


export default EmployeeDistributionOfStudentHeader;