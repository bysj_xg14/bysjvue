export const QuestionerTeamHeader = [
     {
        type: 'selection',
        width: 60,
        align: 'center'
    },
    {
        title: '序号',
        type: 'index',   //列类型
        align: 'center' //字体居中
    },
      {
        width: 1,
        align: 'center',
        key: 'id',
        render: (h, params) => {
          return h('div', {
            style: {
              display: false
            }
          });
        },
        className: 'hidden'
    }, 
	{ 
		title: '答辩组长',
		key: 'leader',
		align: 'center',
		editable: true,
		sortable: false,
		cellType: 'object_leader',
	},
	{
		title: '答辩老师',
		key: 'supervisors',
		align: 'center',
		editable: true,
		sortable: false,
		cellType: 'mobject_questionTeam',
	},
	{
		title: '答辩学生',
		key: 'students',
		align: 'center',
		editable: true,
		sortable: false,
		cellType: 'mobject_questionTeam',
	},
	// {
	// 	title: '所属教研室',
	// 	key: 'department',
	// 	align: 'center',
	// 	editable: true,
	// 	sortable: false,
	// 	cellType: 'object',
	// },
    {
        title: '操作',
        align: 'center',
        key: 'handle',  // 定义handle关键字
        handle: ['edit', 'delete']
    }
];


export default QuestionerTeamHeader;