export const QuestionerTeamHeader = [
   
    
    {
      width: 1,
      align: 'center',
      key: 'id',
      className: 'hidden'
    },
    {
      title: '答辩组长',
      width: 100,
      key: 'leader.value',
      align: 'center',
      editable: false,
      sortable: false,
      cellType: 'object_common',
    },
    {
      title: '答辩老师',
      key: 'supervisors.value',
      align: 'center',
      editable: false,
      sortable: false,
      cellType: 'object_common_multi',
    },
    {
      title: '答辩学生',
      key: 'students.value',
      align: 'center',
      editable: false,
      sortable: false,
      cellType: 'object_common_multi',
    },
    {
      title: '开始时间',
      width: 140,
      key: 'startTime',
      align: 'center',
      editable: false,
      sortable: false,
      cellType: '',
    },
    {
      title: '结束时间',
      width: 140,
      key: 'endTime',
      align: 'center',
      editable: false,
      sortable: false,
      cellType: '',
    },
    {
      title: '答辩地点',
      key: 'address',
      align: 'center',
      editable: false,
      sortable: false,
      cellType: '',
    },
    
  ];
  
  
  export default QuestionerTeamHeader;
  