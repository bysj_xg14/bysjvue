export const MissionDocHeader = [ 
    {
		width: 1,
        align: 'center',
		key: 'id',
		render: (h, params) => {
            return h('div', {
                    style: {
                       display: false
                    }
                });
		},
		className:'hidden'
	},
	{
		title: '题目名称',
		width:200,
		key: 'graduateProject.title',
		align: 'center',
		editable: false,
		sortable: false,
		cellType: 'object_common',
	},
	{
		title: '副标题',
		key: 'graduateProject.subTitle',
		align: 'center',
		editable: false,
		sortable: false,
		cellType: 'object_common',
	},
	{
		title: '题目类型',
		key: 'graduateProject.category',
		width:100,
		align: 'center',
		editable: false,
		sortable: false,
		cellType: 'object_common',
	},
	{
		title: '姓名',
		key: 'value',
		align: 'center',
		editable: false,
		sortable: false,
		cellType: '',
	},
	// {
	// 	title: '班级',
	// 	key: 'studentClass.value',
	// 	align: 'center',
	// 	editable: false,
	// 	sortable: false,
	// 	cellType: 'object_common',
	// },
	{
		title: '学号',
		key: 'idNo',
		align: 'center',
		editable: false,
		sortable: false,
		cellType: '',
	},
	{
		title: '详情',
		key: 'handle',
		align: 'center',
		handle:['showMissionDoc']
	},
	{   
		title: '状态',
		align: 'center',
        key: 'handle',  // 定义handle关键字
		handle: ['showMissionDocState']
	},
    {
        title: '操作',
        align: 'center',
        key: 'handle',  // 定义handle关键字
        handle: ['downloadMissionDoc']
    }
];


export default MissionDocHeader;