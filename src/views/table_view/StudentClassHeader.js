export const StudentClassHeader = [
    {
        type: 'selection',
        width: 60,
        align: 'center'
    },
    {
		width: 1,
        align: 'center',
		key: 'id',
		render: (h, params) => {
            return h('div', {
                    style: {
                       display: false
                    }
                });
        }
	},
	{
		title: '编号',
		key: 'no',
		align: 'center',
		editable: true,
		sortable: false,
		cellType: '',
	},
	{
		title: '班级',
		key: 'value',
		align: 'center',
		editable: true,
		sortable: false,
		cellType: '',
	},
	{
		title: '专业',
		key: 'major',
		align: 'center',
		editable: true,
		sortable: false,
		cellType: 'object',
	},

    {
        title: '操作',
        align: 'center',
        key: 'handle',  // 定义handle关键字
        handle: ['edit', 'delete']
    }
];


export default StudentClassHeader;