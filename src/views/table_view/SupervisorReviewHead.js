export const SupervisorReviewHead = [
    {
        title: '学号',
        width: 160,
        key: 'student.idNo',
        align: 'center',
        sortable: false,
        cellType: 'object_common',
      },
    {
      title: '姓名',
      key: 'student.value',
      align: 'center',
      sortable: false,
      cellType: 'object_common',
    },
    {
      width: 350,
      title: '课题',
      key: 'title',
      align: 'center',
      sortable: false,
      cellType: ''
    },
    {
      title: '年份',
      key: 'year',
      align: 'center',
      sortable: false,
      cellType: '',
    },
    {
      
      title:'题目类别',
          key:'category', 
      align:'center',
      cellType:'',
    },
    {
      title: '论文终稿',
      align: 'center',
      key: 'handle',  // 定义handle关键字
      handle:['downHrefOutCome']
    },
    {
      title: '指导教师评审', 
      align: 'center',
      key: 'evaluationReport', 
      handle: ['review']
    }
  ];
  
  
  export default SupervisorReviewHead;
  