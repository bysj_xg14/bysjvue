export const ReviewReportHeader = [
  {
    title: '序号',
    width: 60,
    type: 'index',   //列类型
    align: 'center', //字体居中
    className: "ive-table-th"
  },
  {
    width: 1,
    align: 'center',
    key: 'id',
    className: 'hidden'
  },
  {
    title: '学号',
    width: 120,
    key: 'student.idNo',
    align: 'center',
    editable: false,
    sortable: false,
    cellType: 'object_common',
    className: "ive-table-th"
  },
  {
    title: '姓名',
    width: 80,
    key: 'student.value',
    align: 'center',
    editable: false,
    sortable: false,
    cellType: 'object_common',
    className: "ive-table-th"
  },
  {
    title: '课题',
    width: 300,
    key: 'title',
    align: 'center',
    editable: false,
    sortable: false,
    cellType: ''
  },
  {
    title: '年份',
    width: 50,
    key: 'year',
    align: 'center',
    editable: false,
    sortable: false,
    cellType: '',
    className: "ive-table-th"
  },
  {
    title: '类别',
    width: 50,
    key: 'category',
    align: 'center',
    editable: false,
    sortable: false,
    cellType: '',
    className: "ive-table-th"
  },
  {
    title: '论文终稿',
    key: 'outcomeReport',
    align: 'center',
    editable: false,
    sortable: false,
    cellType: 'download',
  },
  {
    title: '查看指导老师评审表',
    key: 'evaluationReport',
    align: 'center',
    editable: false,
    sortable: false,
    cellType: 'export',
  },
  {
    title: '评阅人评审',
    align: 'center',
    key: 'reviewReport',
    editable: false,
    sortable: false,
    handle: ["handleOrExport"]
  }
];


export default ReviewReportHeader;
