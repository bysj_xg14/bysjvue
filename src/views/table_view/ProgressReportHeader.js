//指导教师审阅各阶段成果表的表头
export const ProgressReportHeader=[
{  
    title: '序号',
    type: 'index',   //列类型
    align: 'center' //字体居中
},
{
    width: 1,
    align: 'center',
    key: 'id',
    render: (h, params) => {
        return h('div', {
                style: {
                display: false
                }
            });
    },
    className: 'hidden'
},
{   width: 300,
    title: '课题名称',
    key: 'title',
    align: 'center',
    sortable: false,
    cellType: '',
},
{  
    title: '副标题',
    key: 'subTitle',
    align: 'center',
    sortable: false,
    cellType: '',
},
{   
    title: '年份',
    key: 'year',
    align: 'center',
    sortable: false,
    cellType: '',
},
{
 
    title:'题目类别',
    key:'category', 
    align:'center',
    editable: false,
    cellType:'',
},
{   
    title: '姓名',
    key: 'student.value',
    align: 'center',
    sortable: false,
    cellType: 'object_common',
},
{  
    title: '班级',
    key: 'student.studentClass.value',
    align: 'center',
    sortable: false,
    cellType: 'object_common',
},
{ 
    title: '学号',
    key: 'student.idNo',
    align: 'center',
    sortable: false,
    cellType: 'object_common',
},
{
    title: '操作',
    align: 'center',
    key: 'handle',  // 定义handle关键字
    handle: ['check']
}
];
export default  ProgressReportHeader;