export const ThesisSubsidiaryHeader = [
  {
    title: '序号',
    width: 40,
    type: 'index',   //列类型
    align: 'center', //字体居中
    className: "ive-table-th"
  },
  {
    width: 1,
    align: 'center',
    key: 'id',
    className: 'hidden'
  },
  {
    title: '学号',
    key: 'student.idNo',
    align: 'center',
    editable: false,
    sortable: false,
    cellType: 'object_common',
    className: "ive-table-th"
  },
  {
    title: '姓名',
    width: 50,
    key: 'student.value',
    align: 'center',
    editable: false,
    sortable: false,
    cellType: 'object_common',
    className: "ive-table-th"
  },
  {
    title: '课题',
    width: 180,
    key: 'title',
    align: 'center',
    editable: false,
    sortable: false,
    cellType: ''
  },
  {
    title: '类别',
    width: 50,
    key: 'category',
    align: 'center',
    editable: false,
    sortable: false,
    cellType: '',
    className: "ive-table-th"
  },
  {
    title: '任务书',
    width: 50,
    key: 'missionDoc.file',
    align: 'center',
    editable: false,
    sortable: false,
    cellType: 'download',
    className: "ive-table-th"
  },
  {
    title: '开题报告',
    width: 60,
    key: 'thesisProposal.file',
    align: 'center',
    editable: false,
    sortable: false,
    cellType: 'download',
    className: "ive-table-th"
  },
  {
    title: '工作进程表',
    width: 80,
    key: 'schedules',
    align: 'center',
    editable: false,
    sortable: false,
    cellType: 'export',
    className: "ive-table-th"
  },
  {
    title: '指导老师评审表',
    key: 'evaluationReport',
    align: 'center',
    editable: false,
    sortable: false,
    cellType: 'export',
    className: "ive-table-th"
  },
  {
    title: '评阅人评审表',
    key: 'reviewReport',
    align: 'center',
    editable: false,
    sortable: false,
    cellType: 'export',
    className: "ive-table-th"
  },
  {
    title: '答辩小组意见表',
    key: 'questionerTeamReport',
    align: 'center',
    editable: false,
    sortable: false,
    cellType: 'export',
    className: "ive-table-th"
  },
  {
    title: '终稿',
    width: 50,
    key: 'outcomeReport',
    align: 'center',
    editable: false,
    sortable: false,
    cellType: 'download',
    className: "ive-table-th"
  },
  {
    title: '详情',
    align: 'center',
    key: 'handle',  // 定义handle关键字
    handle: ['show']
  }
];


export default ThesisSubsidiaryHeader;
