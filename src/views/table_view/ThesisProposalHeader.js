//只有论文的有开题报告
export const ThesisProposalHeader = [
    {
		width: 1,
        align: 'center',
		key: 'id',
		render: (h, params) => {
            return h('div', {
                    style: {
                       display: false
                    }
                });
		},
		className: 'hidden'
	},
	{   width:300,
        title: '操作',
        align: 'center',
        key: 'handle',  
		handle: ['passSwitch']
    },
	{
		title: '题目名称',
		key: 'title',
		align: 'center',
		editable: false,
		sortable: false,
		cellType: '',
	},
	{   width: 150,
		title: '副标题',
		key: 'subTitle',
		align: 'center',
		editable: false,
		sortable: false,
		cellType: '',
	},
	{
		title: '姓名',
		key: 'student.value',
		align: 'center',
		sortable: false,
		cellType: 'object_common',
	},
	{
		title: '班级',
		key: 'student.studentClass.value',
		align: 'center',
		sortable: false,
		cellType: 'object_common',
	},
	{
		title: '学号',
		key: 'student.idNo',
		align: 'center',
		sortable: false,
		cellType: 'object_common',
	},
	{
		title:'查看开题报告',
		key:'thesisProposal',
		align:'center',
		handle:['showDetail']
	},
	
];


export default ThesisProposalHeader;