//给学生下达任务书的表头
export const MissionDocFormTeacherHeader = [
	{
		width: 1,
        align: 'center',
		key: 'id',
		render: (h, params) => {
            return h('div', {
                    style: {
                       display: false
                    }
                });
		},
		className: 'hidden'
	},
	{   width:300,
		title: '课题名称',
		key: 'title',
		align: 'center',
		sortable: false,
		cellType: '',
	},
	{    
		title: '班级',
		key: 'student.studentClass.value',
		align: 'center',
		sortable: false,
		cellType: 'object_common',
	},
	{   
		title: '学号',
		key: 'student.idNo',
		align: 'center',
		sortable: false,
		cellType: 'object_common',
	},
	{   
		title: '姓名',
		key: 'student.value',
		align: 'center',
		sortable: false,
		cellType: 'object_common',
	},
	{   
		title: '状态',
		key: 'missionDoc',
		align: 'center',
		sortable: false,
		cellType: 'missionDoc_submit',
	},
    {   
        title: '操作',
		align: 'center',
        key: 'handle',  
        handle: ['upload']
    }
];


export default  MissionDocFormTeacherHeader;