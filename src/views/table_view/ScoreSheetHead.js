export const ScoreSheetHead = [
    {
        title: '学号',
        width: 160,
        key: 'student.idNo',
        align: 'center',
        sortable: false,
        cellType: 'object_common',
      },
    {
      title: '姓名',
      key: 'student.value',
      align: 'center',
      sortable: false,
      cellType: 'object_common',
    },
    {
      width: 350,
      title: '课题',
      key: 'title',
      align: 'center',
      sortable: false,
      cellType: ''
    },
    {
      title: '年份',
      key: 'year',
      align: 'center',
      sortable: false,
      cellType: '',
    },
    {
      
      title:'题目类别',
      key:'category', 
      align:'center',
      cellType:'',
    },
    {
        title: '导出成绩表',
        align: 'center',
        editable: false,
        sortable: false,
        cellType: 'exportScoreSheet',
      },
  ];
  
  
  export default ScoreSheetHead;
  