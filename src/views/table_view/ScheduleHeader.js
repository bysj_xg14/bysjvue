export const ScheduleHeader = [
    {
        type: 'selection',
        width: 60,
        align: 'center'
    },
    {
        title: '序号',
        type: 'index',   //列类型
		align: 'center', //字体居中
		width:100,
	},
	//隐藏id
    {
		width: 1,
        align: 'center',
		key: 'id',
		render: (h, params) => {
            return h('div', {
                    style: {
                       display: false
                    }
                });
		},
		className:'hidden'
    },

	{
		title: '开始时间',
		key: 'startDate',
		align: 'center',
		editable: false,
		sortable: false,
		//cellType: 'datePicker',
		// render (row, column, index) {
		// 	return `<strong>${row.name}</strong>`;
		// }
	},
	{
		title: '结束时间',
		key: 'endDate',
		align: 'center',
		editable: false,
		sortable: false,
		//cellType: 'datePicker',
	},
	{
		title: '完成的工作内容',
		key: 'workToDo',
		align: 'center',
		width:180,
		editable: false,
		sortable: false,
		cellType: '',
	},
	
	{
		title: '评语',
		key: 'comment',
		align: 'center',
		editable: false,
		sortable: false,
		cellType: '',
	},
	{
		title: '详情',
		key: 'handle',
		align: 'center',
		handle:['showSchedule'],
	},
	
    // {
    //     title: '操作',
    //     align: 'center',
    //     key: 'handle',  // 定义handle关键字
    //     handle: ['edit', 'delete']
    // }
];


export default ScheduleHeader;