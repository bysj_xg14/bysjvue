export const DegreeHeader = [
  {
    type: 'selection',
    width: 40,
    align: 'center',
    className: "ive-table-th-checkbox"
  },
  {
    width: 1,
    align: 'center',
    key: 'id',
    className: "ive-table-th"
  },
  {
    title: '编号',
    key: 'no',
    align: 'center',
    editable: true,
    sortable: false,
    cellType: '',
  },
  {
    title: '学位',
    key: 'value',
    align: 'center',
    editable: true,
    sortable: false,
    cellType: '',
  },
  {
    title: '操作',
    align: 'center',
    key: 'handle',  // 定义handle关键字
    handle: ['edit', 'delete']
  }
];


export default DegreeHeader;
