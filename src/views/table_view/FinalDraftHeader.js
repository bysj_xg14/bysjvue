export const OutcomeReportHead = [
	{
		title: '题目名称',
	},
	{
		title: '副标题',
	},
	{
		title: '班级',
	},
	{
		title: '学生姓名',
	},
	{
		title: '学号',
    },
    {
		title: '指导老师',
	},
	{
		title: '状态',
	},

];


export default OutcomeReportHead;