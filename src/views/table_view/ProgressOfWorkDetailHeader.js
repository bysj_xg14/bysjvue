export const ProgressOfWorkDetailHeader = [
	{
		//width: 100,
		title: '开始时间',
		key: 'startDate',
		align: 'center',
		sortable: false,
		cellType: '',
	},
	{
		//width: 100,
		title: '结束时间',
		key: 'endDate',
		align: 'center',
		sortable: false,
		cellType: '',
	},
	{
		width: 350,
		title: '完成工作内容',
		key: 'workToDo',
		align: 'center',
		sortable: false,
		cellType: '',
	},
	{   width: 350,
		title: '评语',
		key: 'comment',
		align: 'center',
		editable: true,
		sortable: false,
		cellType: 'textarea',
	},
  /*  {
		width: 100,
        title: '操作',
        align: 'center',
        key: 'handle',  // 定义handle关键字
        handle: ['edit']
	}*/
];


export default ProgressOfWorkDetailHeader;