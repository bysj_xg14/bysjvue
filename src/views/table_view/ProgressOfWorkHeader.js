export const ProgressOfWorkHeader = [
    {
		width: 100,
        title: '序号',
        type: 'index',   //列类型
        align: 'center' //字体居中
    },
	{
		width: 400,
		title: '题目名称',
		key: 'title',
		align: 'center',
		sortable: false,
		cellType: '',
	},
	{
		title: '题目类型',
		key: 'graduateProjectType',
		align: 'center',
		sortable: false,
		cellType: 'object',
	},
	{   
		title: '姓名',
		key: 'student.value',
		align: 'center',
		sortable: false,
		cellType: 'object_common',
	},
	{    
		title: '班级',
		key: 'student.studentClass.value',
		align: 'center',
		sortable: false,
		cellType: 'object_common',
	},
	{   
		title: '学号',
		key: 'student.idNo',
		align: 'center',
		sortable: false,
		cellType: 'object_common',
	},
	{
		//width: 160,
		title: '年份',
		key: 'year',
		align: 'center',
		sortable: false,
		cellType: '',
	},
    {
		//width: 180,
        title: '操作',
        align: 'center',
        key: 'handle',  // 定义handle关键字
        handle: ['check']
    }
];


export default ProgressOfWorkHeader;