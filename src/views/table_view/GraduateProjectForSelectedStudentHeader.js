export const GraduateProjectForStudentHeader = [
    {
		width: 100,
        title: '序号',
        type: 'index',   //列类型
        align: 'center' //字体居中
    },
    {
		width: 1,
        align: 'center',
		key: 'id',
		render: (h, params) => {
            return h('div', {
                    style: {
                       display: false
                    }
                });
		},
		className: 'hidden'
    },
	{
		width: 450,
		title: '题目名称',
		key: 'title',
		align: 'center',
		sortable: false,
		cellType: '',
	},
	{
		//width: 160,
		title: '题目类型',
		key: 'graduateProjectType',
		align: 'center',
		sortable: false,
		cellType: 'object',
	},
	{
		//width: 160,
		title: '年份',
		key: 'year',
		align: 'center',
		sortable: false,
		cellType: '',
	},
	{
		title: '学生',
		key: 'student',
		align: 'center',
		sortable: false,
		cellType: 'object',
	},
    {
		//width: 180,
        title: '操作',
        align: 'center',
        key: 'handle',  // 定义handle关键字
        handle: ['unmatch']
    }
];


export default GraduateProjectForStudentHeader;