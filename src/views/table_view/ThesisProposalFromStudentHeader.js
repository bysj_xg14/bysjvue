export const ThesisProposalFromStudentHeader = [
    
    {
        title: '序号',
        type: 'index',   //列类型
		align: 'center', //字体居中
		width:100,
	},
	//隐藏id
    {
		width: 1,
        align: 'center',
		key: 'id',
		render: (h, params) => {
            return h('div', {
                    style: {
                       display: false
                    }
                });
		},
		className:'hidden'
    },

	// {
	// 	title: '题目名称',
	// 	key: 'graduateProject.title',
	// 	align: 'center',
	// 	editable: false,
	// 	sortable: false,
	// 	cellType: 'object_common',
	// },
	
	{
		title: '附件名',
		key: 'file',
		align: 'center',
		width:180,
		editable: false,
		sortable: false,
		cellType: 'file_name',
	},
	
	{
		title: '指导老师审核',
		key: 'approvalFromSupervisor.approvalState.value',
		align: 'center',
		editable: false,
		sortable: false,
		cellType: 'object_common',
    },
    // {
	// 	title: '教研室主任审核',
	// 	key: 'approvalFromDirector.approvalState.value',
	// 	align: 'center',
	// 	editable: false,
	// 	sortable: false,
	// 	cellType: 'object_common',
	// },
	{
        title: '状态',
        align: 'center',
        key: 'handle',  // 定义handle关键字
        handle: ['uploadOrNot']//可根据审核状态改变
    },
    {
        title: '操作',
        align: 'center',
        key: 'handle',  // 定义handle关键字
        handle: ['showOrDeleteOrDown']//可根据审核状态改变
    }
];


export default ThesisProposalFromStudentHeader;