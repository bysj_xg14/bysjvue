export const StudentHeader = [
  {
    type: 'selection',
    width: 40,
    align: 'center',
    className: "ive-table-th-checkbox"
  },
  {
    title: '序号',
    width: 60,
    type: 'index',   //列类型
    align: 'center', //字体居中
    className: "ive-table-th"
  },
  {
    title: '学号',
    key: 'idNo',
    align: 'center',
    editable: true,
    sortable: true,
    cellType: '',
  },
  {
    title: '姓名',
    width: 100,
    key: 'value',
    align: 'center',
    editable: true,
    sortable: false,
    cellType: '',
    className: "ive-table-th"
  },
  {
    title: '班级',
    key: 'studentClass',
    align: 'center',
    editable: true,
    sortable: false,
    cellType: 'object',
  },
  {
    title: '邮箱',
    key: 'email',
    align: 'center',
    editable: true,
    sortable: false,
    cellType: '',
  },
  {
    title: 'QQ',
    key: 'qq',
    align: 'center',
    editable: true,
    sortable: false,
    cellType: '',
  },


  {
    title: '操作',
    width: 150,
    align: 'center',
    key: 'handle',  // 定义handle关键字
    handle: ['edit', 'delete', 'reset']
  }
];


export default StudentHeader;
