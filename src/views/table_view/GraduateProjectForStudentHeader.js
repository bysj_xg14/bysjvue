export const GraduateProjectForStudentHeader = [
    {
		width: 100,
        title: '序号',
        type: 'index',   //列类型
        align: 'center' //字体居中
    },
	{
		width: 500,
		title: '题目名称',
		key: 'title',
		align: 'center',
		sortable: false,
		cellType: '',
	},
	{
		//width: 160,
		title: '题目类型',
		key: 'graduateProjectType',
		align: 'center',
		sortable: false,
		cellType: 'object',
	},
	{
		//width: 160,
		title: '年份',
		key: 'year',
		align: 'center',
		sortable: false,
		cellType: '',
	},
    {
		//width: 180,
        title: '操作',
        align: 'center',
        key: 'handle',  // 定义handle关键字
        handle: ['match']
    }
];


export default GraduateProjectForStudentHeader;