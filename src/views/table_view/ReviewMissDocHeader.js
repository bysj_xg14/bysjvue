export const ReviewTopicHeader = [
    {
        type: 'selection',
        width: 60,
        align: 'center'
    },
    {
        width: 1,
        align: 'center',
        key: 'id',
        render: (h, params) => {
          return h('div', {
            style: {
              display: false
            }
          });
        },
        className: 'hidden'
    }, 
    {
        title: '序号',
        type: 'index',   //列类型
        align: 'center' //字体居中
    },
    {
        title: '题目名称',
        key: 'graduateProject',
        align: 'center',
        editable: false,
        sortable: false,
        cellType: 'object_title',
    },
     {
        title: '副标题',
        key: 'graduateProject.subTitle',
        align: 'center',
        editable: false,
        sortable: false,
        cellType: 'object_common',
    },
    {
        title: '学生姓名',
        key: 'graduateProject.student.value',
        align: 'center',
        editable: false,
        sortable: false,
        cellType: 'object_common',
    },
    {
        title: '学生学号',
        key: 'graduateProject.student.idNo',
        align: 'center',
        editable: false,
        sortable: false,
        cellType: 'object_common',
    },
    {
        title: '学生班级',
        key: 'graduateProject.student.studentClass.value',
        align: 'center',
        editable: false,
        sortable: false,
        cellType: 'object_common',
    },
    {
        title: '题目类型',
        key: 'graduateProject.graduateProjectType.value',
        align: 'center',
        editable: false,
        sortable: false,
        cellType: 'object_common',
    },
    {
        title: '题目性质',
        key: 'graduateProject.graduateProjectNature.value',
        align: 'center',
        editable: false,
        sortable: false,
        cellType: 'object_common',
    },
    {
        title: '教研室主任审核状况',
        key: 'approvalFromDirector.approvalState.value',
        align: 'center',
        editable: false,
        sortable: false,
        cellType: 'object_common'
    },
    {
        title: '操作',
        key: 'approvalState',
        align: 'center',
        editable: true,
        sortable: false,
        cellType: 'object_edit'
    },
    {
        title: '详情',
        align: 'center',
        key: 'handle',  // 定义handle关键字
        handle: ['show']
    }
    
];


export default ReviewTopicHeader;