import Vue from 'vue'
import Router from 'vue-router'
import index from '@/views/layout-components/index.vue'
import timeAndPlace from '@/views/table_view/ManageQuestionerTeamTableBody.vue'
import thesisSubsidiary from '@/views/table_view/ThesisSubsidiaryTableBody.vue'
import employeeMessage from '@/views/table_view/EmployeeTableBody.vue'
import visitingEmployeeMessage from '@/views/table_view/VisitingEmployeeTableBody.vue'
import studentMessage from '@/views/table_view/StudentTableBody.vue'
import basicMessage from '@/views/table_view/SchoolTableBody.vue'
import dbfz from '@/views/table_view/QuestionerTeamTableBody.vue'
import fpxs from '@/views/table_view/DistributionOfStudentTableBody.vue'
import shtm from '@/views/table_view/ReviewTopicTableBody.vue'
import shrws from '@/views/table_view/ReviewMissDocTableBody.vue'

import checkTimeAndPlace from '@/views/table_view/CheckTimeAndPlaceTableBody.vue'
import missionDoc from '@/views/table_view/MissionDocTableBody.vue'
import thesisProposal from '@/views/table_view/ThesisProposalFromStudent.vue'
import thesisProposalRouter from '@/views/table_view/ThesisProposalRouterFromStudent.vue'
import schedule from '@/views/table_view/ScheduleTableBody.vue'
import progressReport from '@/views/table_view/ProgressReportByStudent.vue'
import fdu from '@/views/table_view/FinalDraftTableBody.vue'
import xglsbysj from '@/views/table_view/updateGraduateProjectFromDirectorTableBody.vue'
import pyrgl from '@/views/table_view/ReviewerManagementTableBody.vue'

import login from '@/views/login.vue'
import wsbdtm from '@/views/table_view/ShowTeacherGraduateProject.vue'
import cxxytm from '@/views/table_view/GraduateProjectFromTeachersTableBody.vue'
import xdrws from '@/views/table_view/MissionDocFormTeacherTableBody.vue'
import xdrwsRouter from '@/views/table_view/MissionDocFormTeacherRouterTableBody.vue'
import shktbg from '@/views/table_view/ApprovalThesisProposalTableBody.vue'
import reviewReport from "@/views/table_view/ReviewReportTableBody.vue"
import profTitleMessage from "@/views/table_view/profTitleTableBody.vue"
import departmentMessage from "@/views/table_view/DepartmentTableBody.vue"
import majorMessage from "@/views/table_view/MajorTableBody.vue"
import classMessage from "@/views/table_view/StudentClassTableBody.vue"
import fptm from "@/views/table_view/GraduateProjectFortudentTableBody.vue"
import questionerTeamReport from "@/views/table_view/QuestionerTeamReportTableBody.vue"
import answerQuestionsReport from "@/views/table_view/AnswerQuestionsReportTableBody.vue"
import sygjdcg from "@/views/table_view/ProgressReportTableBody.vue"
import zdlspsb from "@/views/table_view/SupervisorReviewTable.vue"
import cjpfb from "@/views/table_view/ScoreSheetTablebody.vue"
import shgzjcb from "@/views/table_view/ProgressOfWorkTableBody.vue"
import degreeBasic from "@/views/table_view/DegreeTableBody.vue"
import profTitleBasic from "@/views/table_view/ProfTitleBasicTableBody.vue"
import ckktbg from "@/views/table_view/ShowThesisProposalTableBody.vue"
Vue.use(Router);

export default new Router({
  mode: 'history',
  base: '/',
  routes: [
    {
      path: '/login',
      component: login
    },
    {
      path: '/',
      component: index
    },
    //院级管理员
    //选题流程
    {
      path: '/',
      component: index,
      children: [
        {
          path: 'timeAndPlace',
          name: '安排答辩时间地点',
          component: timeAndPlace
        },
        {
          path: 'thesisSubsidiary',
          name: '查看论文明细表',
          component: thesisSubsidiary
        }
      ]
    },
    //用户管理
    {
      path: '/',  //校内老师
      component: index,
      children: [
        {
          path: 'employeeMessage',
          name: '查看老师信息',
          component: employeeMessage
        }
      ]
    },
    {
      path: '/',  //校外老师
      component: index,
      children: [
        {
          path: 'visitingEmployeeMessage',
          name: '查看校外老师信息',
          component: visitingEmployeeMessage
        }
      ]
    },
    {
      path: '/',  //学生信息
      component: index,
      children: [
        {
          path: 'studentMessage',
          name: '查看学生信息',
          component: studentMessage
        }
      ]
    },
    {
      path: '/',  //基本信息管理
      component: index,
      children: [
        {
          path: 'basicMessage',
          name: '基本信息管理',
          component: basicMessage
        }, {
          path: 'departmentMessage',
          name: '教研室管理',
          component: departmentMessage
        }, {
          path: 'majorMessage',
          name: '专业管理',
          component: majorMessage
        }, {
          path: 'classMessage',
          name: '班级管理',
          component: classMessage
        }
      ]
    },
    {
      path: '/',  //教师职位管理
      component: index,
      children: [
        {
          path: 'profTitleMessage',
          name: '教师职位管理',
          component: profTitleMessage
        }
      ]
    },
    //基础信息维护
    {
      path: '/',  //学历维护
      component: index,
      children: [
        {
          path: 'degreeBasic',
          name: '学位维护',
          component: degreeBasic
        }
      ]
    },
    {
      path: '/',  //职称维护
      component: index,
      children: [
        {
          path: 'profTitleBasic',
          name: '职称维护',
          component: profTitleBasic
        }
      ]
    },
    //教研室主任
    {
      path: '/',  //答辩分组安排
      component: index,
      children: [
        {
          path: 'dbfz',
          name: '答辩分组安排',
          component: dbfz
        }
      ]
    },
    {
      path: '/',  //分配学生
      component: index,
      children: [
        {
          path: 'fpxs',
          name: '分配学生',
          component: fpxs
        }
      ]
    },
    {
      path: '/',  //审核题目
      component: index,
      children: [
        {
          path: 'shtm',
          name: '审核题目',
          component: shtm
        }
      ]
    },
    {
      path: '/',  //审核任务书
      component: index,
      children: [
        {
          path: 'shrws',
          name: '审核任务书',
          component: shrws
        }
      ]
    },
    // xglsbysj
     {
      path: '/',  //教研室主任修改老师申报的题目
      component: index,
      children: [
        {
          path: 'xglsbysj',
          name: '修改题目',
          component: xglsbysj
        }
      ]
    },
    {
      path: '/',  //教研室主任为课题指定评阅人
      component: index,
      children: [
        {
          path: 'pyrgl',
          name: '评阅人管理',
          component: pyrgl
        }
      ]
    },
    // 指导老师
    {//选课流程
      path: '/',
      component: index,
      children: [
        {
          path: 'cxxytm',
          name: '查询现有题目',
          component:cxxytm
        },
        {
          path: 'wsbdtm',
          name: '我申报的题目',
          component:wsbdtm
        },
        {
          path: 'fptm',
          name: '分配题目',
          component:fptm
        },

      ]
    },
     {//指导流程
      path:'/',
      component:index,
      children:[
        {
          path: 'xdrws',
          name: '下达任务书',
          component:xdrws
        },
        {
          path: 'xdrwsRouter',
          name: '下达任务书',
          component:xdrwsRouter
        },
        {
          path: 'shktbg',
          name: '审核开题报告',
          component:shktbg
        },
        {
          path: 'ckktbg',
          name: '查看开题报告',
          component:ckktbg
        },
        {
           path: 'shgzjcb',
           name: '审核工作进程表',
           component:shgzjcb
         },
         {
          path: 'sygjdcg',
          name: '审阅各阶段成果',
          component:sygjdcg
        }
      ]
    },
     {//指导老师评审流程
      path:'/',
      component:index,
      children:[
        {
          path: 'zdlspsb',
          name: '指导老师评审表',
          component:zdlspsb
        },
        {
          path: 'cjpfb',
          name: '成绩评分表',
          component:cjpfb
        },
      ]
    },
    {
      path: '/',
      component: index,
      children: [
        {
          path: 'reviewReport',
          name: "评阅人评审表",
          component: reviewReport

        }
      ]
    },
    {//答辩组长
      path: "/",
      component: index,
      children: [
        {
          path: 'questionerTeamReport',
          name: "答辩小组意见",
          component: questionerTeamReport
        }
      ]
    },
    {//答辩老师
      path: "/",
      component: index,
      children: [
        {
          path: 'answerQuestionsReport',
          name: "答辩提问录",
          component: answerQuestionsReport
        }
      ]
    },
    // 学生checkTimeAndPlace
    {
      path: '/',  //查看答辩时间地点
      component: index,
      children:[
        {
          path: 'checkTimeAndPlace',
          name: '查看答辩时间地点',
          component:checkTimeAndPlace
        }
      ]
    },
    {
      path: '/',  //查看任务书
      component: index,
      children:[
        {
          path: 'missionDoc',
          name: '查看任务书',
          component:missionDoc
        }
      ]
    },
    {
      path: '/',  //上传开题报告
      component: index,
      children:[
        {
          path: 'thesisProposal',
          name: '上传开题报告',
          component:thesisProposal
        },
        {
          path: 'thesisProposalRouter',
          name: '上传开题报告',
          component:thesisProposalRouter
        }
      ]
    },
    {
      path: '/',  //填写工作进程
      component: index,
      children:[
        {
          path: 'schedule',
          name: '填写工作进程',
          component:schedule
        }
      ]
    },
    {
      path: '/',  //上传阶段成果
      component: index,
      children:[
        {
          path: 'progressReport',
          name: '上传阶段成果',
          component:progressReport
        }
      ]
    },
    {
      path: '/',  //上传终稿
      component: index,
      children: [
        {
          path: 'fdu',
          name: '上传终稿',
          component:fdu
        }
      ]
    },
  ]
})
